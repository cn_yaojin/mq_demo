package com.cn.demo.listener;

import com.cn.client.IMessageConsumeService;
import com.cn.client.MessageConsumeResultStatus;
import com.cn.common.MessageExt;

import java.nio.charset.Charset;
import java.util.List;

public class YjMqListener implements IMessageConsumeService {

    @Override
    public MessageConsumeResultStatus execute(List<MessageExt> msgs) {
        for (MessageExt msg : msgs) {
            System.out.println("消息内容：" + new String(msg.getBody(), Charset.forName("utf-8")));
        }
        return MessageConsumeResultStatus.commit;
    }
}
