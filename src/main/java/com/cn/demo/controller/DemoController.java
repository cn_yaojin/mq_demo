package com.cn.demo.controller;

import com.cn.mq.MqProducer;
import com.cn.producer.SendResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping(value = "demo")
public class DemoController {

    @Resource(name = "yjMqProducer")
    private MqProducer yjMqProducer;


    @GetMapping(value = "/test")
    public void test() {
        String topic = "topic";
        String content = "hi,cn_yaojin";
        try {
            System.out.println(content);
            SendResult sendResult = this.yjMqProducer.sendMsg(topic, content);
            if (sendResult != null) {
                System.out.println(sendResult.getSendStatus());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
