package com.cn.mq;

import com.cn.file.MessageStore;
import com.cn.producer.Producer;
import com.cn.producer.SendResult;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.nio.charset.Charset;

@Component(value = "yjMqProducer")
public class MqProducer {

    @Resource(name = "mqProducer")
    private Producer mqProducer;

    public SendResult sendMsg(String topic, String content) {
        MessageStore messageStore = new MessageStore();
        messageStore.setTopic("topic").setBody(content.getBytes(Charset.forName("UTF-8")));
        SendResult result = mqProducer.send(messageStore);
        return result;
    }


}
