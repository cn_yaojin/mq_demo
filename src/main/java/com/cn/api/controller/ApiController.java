package com.cn.api.controller;

import com.cn.api.client.ClientController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(value = "/api")
public class ApiController {

    @Resource(name = "clientController")
    private ClientController clientController;

    @GetMapping(value = "/pullAllTopic")
    public Object pullAllTopic() {
        Map<String, Object> pd = new HashMap<>();
        try {
            pd.put("list", this.clientController.queryAllTopic());
            pd.put("result", true);
        } catch (Exception e) {
            e.printStackTrace();
            pd.put("result", false);
        }
        return pd;
    }

}
